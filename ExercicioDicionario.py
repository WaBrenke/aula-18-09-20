cadastro_pessoa = {
    'nome': 'Warley Brenke',
    'idade': 22,
    'anime_favorito': 'Naruto',
    'serie_favorita': 'Billions',
    'time_favorito_fa': 'Miami Dolphins',
    'time_favorito_soccer': 'Corinthians'
}

for chave, valor in cadastro_pessoa.items():
    print(f'{chave} é {valor}')
