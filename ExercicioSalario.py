#Função Salário com bonus

def salario_bonus(nome_vendedor, salario_fixo, vendas):
    comissao = vendas*0.15
    salariofinal = salario_fixo+comissao
    print(f'O salário final do vendedor {nome_vendedor} é de {round(salariofinal,2)}')

salario_bonus('Warley', 1500, 1353)
