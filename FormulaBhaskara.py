#Função para calcular Bhascara

def eq_segundo_grau(a,b,c):
    delta = ((b**2)-(4*a*c))
    raiz_delta = delta**(1/2)
    if delta >= 0:
        x1 = (-b + raiz_delta) / (2 * a)
        x2 = (-b - raiz_delta) / (2 * a)
        print(f'As raízes da equação são: {x1} e {x2}')
    else:
        print('A equação não tem raízes reais')

eq_segundo_grau(1,-3,-10)