#Groger Bank

def cadastrar_cliente(nome, idade):
    cliente = {
        'nome': nome,
        'idade': idade,
    }
    if cliente.get('idade') < 18:
        print('Não foi possível efetuar o cadastro')
    else:
        print('Cadastro realizado com sucesso!')
        return cliente

def depositar(saldo, valor):
    if valor <= 0:
        print('Não foi possível realizar o saque')
    else:
        print('Depósito realizado com sucesso')
        saldo += valor
        return saldo

def sacar(saldo, valor):
    if valor > saldo:
        print('Não foi possível efetuar o saque, pois o valor é maior que o saldo')
        return saldo
    elif valor <= 0:
        print('Valor de saque inválido!')
        return saldo
    elif valor > 900:
        print('Valor acima de 900 reais')
        return saldo
    else:
        saldo = saldo - valor
        print('Saque efetuado com sucesso!')
        return saldo

def pedir_emprestimo(salario, saldo, valor):
    if valor > salario*10:
        print('Empréstimo não realizado, pois é um valor muito alto')
        return saldo
    elif valor <= 0:
        print('Valor de empréstimo inválido!')
        return saldo
    else:
        saldo += valor
        print('Empréstimo realizado com sucesso!')
        return saldo

cliente1 = cadastrar_cliente('Cleytin', 23)
print(cliente1)

saldo_cliente1 = depositar(0, 1500)
print(saldo_cliente1)

saldo_cliente1 = depositar(saldo_cliente1, 1500)
print(saldo_cliente1)

saldo_cliente1 = sacar(saldo_cliente1, 300)
print(saldo_cliente1)

saldo_cliente1 = pedir_emprestimo(1700, saldo_cliente1, 0)
print(saldo_cliente1)